﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CoolParking.WebAPI.Middlewares
{
    public class GlobalExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public GlobalExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception error)
            {
                var response = context.Response;
                response.ContentType = "application/json";
                GetResponseStatusCodeFromException(error.InnerException ?? error, response);
                var result = JsonSerializer.Serialize(new
                {
                    title = "An exception was thrown",
                    status = response.StatusCode,
                    errors = new Dictionary<string, string[]> {{error.GetType().Name, new[] {error.Message}}}
                });
                await response.WriteAsync(result);
            }
        }

        private static void GetResponseStatusCodeFromException(Exception error, HttpResponse response)
        {
            switch (error)
            {
                case FormatException:
                case InvalidOperationException:
                case ArgumentException:
                case BadHttpRequestException:
                    response.StatusCode = (int) HttpStatusCode.BadRequest;
                    break;
                case FileNotFoundException:
                case KeyNotFoundException:
                    response.StatusCode = (int) HttpStatusCode.NotFound;
                    break;
                default:
                    response.StatusCode = (int) HttpStatusCode.InternalServerError;
                    break;
            }
        }
    }
}