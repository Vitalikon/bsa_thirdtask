﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models.DTO.Vehicle;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IParkingService _parkingService;


        public VehiclesController(IParkingService parkingService, IMapper mapper)
        {
            _parkingService = parkingService;
            _mapper = mapper;
        }

        // GET: api/vehicles
        [HttpGet]
        public ActionResult<IEnumerable<VehicleDTO>> GetVehicles()
        {
            return Ok(_mapper.Map<IEnumerable<VehicleDTO>>(_parkingService.GetVehicles()));
        }

        // GET: api/vehicles/{id} (AA-0000-AA)
        [HttpGet("{id}")]
        public ActionResult<VehicleDTO> GetVehicleById([RegularExpression(Settings.IdFormatRegEx)]
            string id)
        {
            return Ok(_mapper.Map<VehicleDTO>(_parkingService.GetVehicle(id)));
        }

        // POST: api/vehicles
        [HttpPost]
        public ActionResult<VehicleDTO> CreateVehicle([FromBody] CreateVehicleDTO model)
        {
            if (model.Balance == null)//try fix
                throw new FormatException("Bad balance format");

            _parkingService.AddVehicle(_mapper.Map<Vehicle>(model));
            return CreatedAtAction("CreateVehicle", "Vehicles", new {id = model.Id}, _mapper.Map<VehicleDTO>(model));
        }


        // DELETE: api/vehicles/{id} (AA-0000-AA)
        [HttpDelete("{id}")]
        public ActionResult DeleteVehicle([RegularExpression(Settings.IdFormatRegEx)]
            string id)
        {
            _parkingService.RemoveVehicle(id);
            return NoContent();
        }
    }
}