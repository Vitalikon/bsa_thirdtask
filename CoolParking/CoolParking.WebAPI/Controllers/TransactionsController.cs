﻿using System;
using System.Collections.Generic;
using AutoMapper;
using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.Models.DTO.Transaction;
using CoolParking.WebAPI.Models.DTO.Vehicle;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class Transactions : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IParkingService _parkingService;

        public Transactions(IParkingService parkingService, IMapper mapper)
        {
            _parkingService = parkingService;
            _mapper = mapper;
        }

        // GET: api/transactions/last
        [HttpGet("last")]
        public ActionResult<IEnumerable<TransactionInfoDTO>> GetLastTransactions()
        {
            return Ok(_mapper.Map<IEnumerable<TransactionInfoDTO>>(_parkingService.GetLastParkingTransactions()));
        }

        // GET: api/transactions/all
        [HttpGet("all")]
        public ActionResult<string> GetAll()
        {
            return Ok(_parkingService.ReadFromLog());
        }

        // PUT: api/transactions/topUpVehicle
        [HttpPut("topUpVehicle")]
        public ActionResult<VehicleDTO> TopUpVehicle([FromBody] TopUpVehicleDTO model)
        {
            if (model.Sum == null)//try fix
                throw new FormatException("Bad sum format");
            _parkingService.TopUpVehicle(model.Id, (decimal) model.Sum);
            return Ok(_mapper.Map<VehicleDTO>(_parkingService.GetVehicle(model.Id)));
        }
    }
}