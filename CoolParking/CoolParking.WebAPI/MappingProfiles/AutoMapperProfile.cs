﻿using AutoMapper;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models.DTO.Transaction;
using CoolParking.WebAPI.Models.DTO.Vehicle;

namespace CoolParking.WebAPI.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            #region Vehicle

            CreateMap<Vehicle, VehicleDTO>().ReverseMap();
            CreateMap<VehicleDTO, CreateVehicleDTO>().ReverseMap();
            CreateMap<CreateVehicleDTO, Vehicle>();

            #endregion

            #region Transaction

            CreateMap<TransactionInfo, TransactionInfoDTO>()
                .ForMember(x => x.TransactionDate,
                    opt =>
                        opt.MapFrom(x => x.Time));
            CreateMap<TransactionInfoDTO, TransactionInfo>()
                .ForMember(x =>
                    x.Time, opt =>
                    opt.MapFrom(x => x.TransactionDate));

            #endregion
        }
    }
}