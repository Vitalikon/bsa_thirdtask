﻿using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Models.DTO.Vehicle
{
    public class VehicleDTO
    {
        public string Id { get; set; }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }
    }
}