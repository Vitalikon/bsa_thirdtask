﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Models.DTO.Vehicle
{
    public class CreateVehicleDTO
    {
        [Required(ErrorMessage = "Id is required!")]
        [RegularExpression(Settings.IdFormatRegEx, ErrorMessage = "Vehicle id does not match format!")]
        public string Id { get; set; }

        [Required(ErrorMessage = "VehicleType is required!")]
        [Range(0, 3, ErrorMessage = "Vehicle type doesn't exist!")]

        public VehicleType? VehicleType { get; set; }

        [Required(ErrorMessage = "Balance is required!")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Vehicle initial balance cannot be less than 0!")]
        [JsonNumberHandling(JsonNumberHandling.Strict)]
        public decimal? Balance { get; set; }
    }
}