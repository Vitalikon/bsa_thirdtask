﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Models.DTO.Vehicle
{
    public class TopUpVehicleDTO
    {
        [Required(ErrorMessage = "Id is required!")]
        [RegularExpression(Settings.IdFormatRegEx, ErrorMessage = "Vehicle id does not match format")]
        public string Id { get; set; }

        [Required(ErrorMessage = "Sum is required")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Sum cannot be less than 0!")]
        [JsonNumberHandling(JsonNumberHandling.Strict)]
        public decimal? Sum { get; set; }
    }
}