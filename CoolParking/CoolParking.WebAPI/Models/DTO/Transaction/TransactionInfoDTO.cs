﻿using System;

namespace CoolParking.WebAPI.Models.DTO.Transaction
{
    public class TransactionInfoDTO
    {
        public string VehicleId { get; set; }
        public decimal Sum { get; set; }
        public DateTime TransactionDate { get; set; }
    }
}