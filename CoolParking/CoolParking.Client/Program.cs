﻿using System;

namespace CoolParking.Client
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var menu = new ConsoleMenu();
            var showMenu = true;
            while (showMenu)
            {
                Console.Clear();
                showMenu = menu.ShowMenu();
                Console.WriteLine("\nPress any button to continue");
                Console.ReadKey();
            }
        }
    }
}