﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using CoolParking.Client.Extensions;
using CoolParking.Client.Models;
using CoolParking.Client.Providers;
using CoolParking.Client.Utilities;

namespace CoolParking.Client
{
    internal class ConsoleMenu
    {
        private readonly ParkingProvider _parkingProvider;
        private readonly TransactionsProvider _transactionsProvider;
        private readonly VehiclesProvider _vehiclesProvider;

        public ConsoleMenu()
        {
            _parkingProvider = new ParkingProvider(new HttpClient(), Settings.BaseDevelopmentUri);
            _transactionsProvider = new TransactionsProvider(new HttpClient(), Settings.BaseDevelopmentUri);
            _vehiclesProvider = new VehiclesProvider(new HttpClient(), Settings.BaseDevelopmentUri);
        }

        public bool ShowMenu()
        {
            try
            {
                Console.WriteLine("1. Show parking balance\n" +
                                  "2. Show profit for the current period\n" +
                                  "3. Show parking capacity\n" +
                                  "4. Show all parking transactions for the current period\n" +
                                  "5. Show transactions history\n" +
                                  "6. Show list of parked vehicles\n" +
                                  "7. Park vehicle\n" +
                                  "8. Remove vehicle from parking\n" +
                                  "9. Get vehicle by ID\n" +
                                  "10. Top up vehicle balance\n" +
                                  "0. Exit");
                Console.Write("\nSelect an option: ");
                var choice = InputValidators.IntegerInput(0, 10);
                Console.Clear();
                Console.WriteLine("Waiting...");
                switch (choice)
                {
                    case 1:
                        ShowParkingBalance();
                        return true;
                    case 2:
                        ShowCurrentProfit();
                        return true;
                    case 3:
                        ShowParkingCapacity();
                        return true;
                    case 4:
                        ShowCurrentParkingTransactions();
                        return true;
                    case 5:
                        ShowTransactionsHistoryFromLogs();
                        return true;
                    case 6:
                        ShowParkedVehicles();
                        return true;
                    case 7:
                        ParkVehicle();
                        return true;
                    case 8:
                        RemoveVehicle();
                        return true;
                    case 9:
                        GetVehicleById();
                        return true;
                    case 10:
                        TopUpVehicle();
                        return true;
                    case 0:
                        return false;
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Error: " + e.Message);
                if (e.InnerException != null)
                    if (e.Message != e.InnerException.Message)
                        Console.WriteLine($"{"Inner Error:" + e.InnerException.Message}");
            }

            return true;
        }


        // 1
        private void ShowParkingBalance()
        {
            var balance = _parkingProvider.GetBalance().GetAwaiter().GetResult();
            Console.Clear();
            Console.WriteLine($"Parking balance: {balance}");
        }

        // 2
        private void ShowCurrentProfit()
        {
            var transactions = _transactionsProvider.GetLastTransactionsAsync().GetAwaiter().GetResult();
            if (transactions == null)
                throw new KeyNotFoundException("No transactions data!");

            Console.Clear();
            Console.WriteLine($"Current profit: {transactions.Sum(t => t.Sum)}");
        }

        // 3
        private void ShowParkingCapacity()
        {
            var getCapacity = _parkingProvider.GetCapacity();
            var getFreePlaces = _parkingProvider.GetFreePlaces();
            Task.WaitAll(getCapacity, getFreePlaces);
            Console.Clear();
            Console.WriteLine($"Capacity: {getCapacity.Result}\n" +
                              $"Free places: {getFreePlaces.Result}");
        }

        // 4
        private void ShowCurrentParkingTransactions()
        {
            var transactions = _transactionsProvider.GetLastTransactionsAsync().GetAwaiter().GetResult();
            Console.Clear();
            if (transactions == null)
                throw new KeyNotFoundException("No transactions data!");
            transactions.ForEach(info => Console.WriteLine(info.ToString()));
        }

        // 5
        private void ShowTransactionsHistoryFromLogs()
        {
            var transactions = _transactionsProvider.GetAllTransactionsAsync().GetAwaiter().GetResult();

            Console.Clear();
            Console.WriteLine($"There is {transactions.Count} transactions. Select an option:\n" +
                              "1. Show 50 transactions page by page\n" +
                              "2. Show last 50 transactions\n" +
                              "3. Show all transactions\n");
            var choice = InputValidators.IntegerInput(1, 3);
            Console.Clear();
            switch (choice)
            {
                case 1:
                    ShowTransactionsWithPagination(transactions);
                    break;
                case 2:
                    transactions.TakeLast(50).ForEach(Console.WriteLine);
                    break;
                case 3:
                    transactions.ForEach(Console.WriteLine);
                    break;
            }
        }


        // 6
        private void ShowParkedVehicles()
        {
            var vehicles = _vehiclesProvider.GetVehicles().GetAwaiter().GetResult();
            if (vehicles == null) throw new KeyNotFoundException("Vehicles not found");

            Console.Clear();
            DisplayVehicleTable(vehicles.ToList());
        }

        // 7
        private void ParkVehicle()
        {
            Console.Clear();
            Console.Write("Enter new vehicle Id (AA-0001-AA): ");
            var vehicleId = InputValidators.StringInput().ToUpper();
            if (!vehicleId.IsMatchVehicleIdStringFormat())
                throw new ArgumentException("Vehicle Id does not match format");

            Console.Write("Choose type of vehicle:\n" +
                          "1 - PassengerCar\n" +
                          "2 - Truck\n" +
                          "3 - Bus\n" +
                          "4 - Motorcycle\n>");
            var vehicleType = (VehicleType) InputValidators.IntegerInput(1, 4) - 1;

            Console.Write("Write initial vehicle balance: ");
            var vehicleBalance = InputValidators.DecimalInput(0.01m);

            var vehicle = _vehiclesProvider.CreateVehicle(new Vehicle
                    {Balance = vehicleBalance, Id = vehicleId, VehicleType = vehicleType})
                .GetAwaiter().GetResult();
            if (vehicle == null)
                throw new SerializationException("Bad vehicle serialization");
            Console.WriteLine($"Vehicle {vehicle.Id} parked.");
        }

        // 8
        private void RemoveVehicle()
        {
            var vehicles = _vehiclesProvider.GetVehicles().GetAwaiter().GetResult();
            if (vehicles == null)
                throw new SerializationException("Bad vehicles serialization");
            Console.Clear();
            DisplayVehicleTable(vehicles.ToList());
            Console.Write("\nSelect vehicle to remove by Id: ");
            var id = InputValidators.StringInput().ToUpper();
            if (!id.IsMatchVehicleIdStringFormat())
                throw new FormatException("Bad id format");

            var isDeleted = _vehiclesProvider.DeleteVehicleById(id).GetAwaiter().GetResult();
            if (!isDeleted) throw new OperationCanceledException("Vehicle not removed");

            Console.WriteLine("Vehicle removed.");
        }

        // 9
        private void GetVehicleById()
        {
            Console.Clear();
            Console.Write("Enter vehicle Id (AA-0001-AA): ");
            var vehicleId = InputValidators.StringInput().ToUpper();
            if (!vehicleId.IsMatchVehicleIdStringFormat())
                throw new ArgumentException("Vehicle Id does not match format");
            var vehicle = _vehiclesProvider.GetVehicleById(vehicleId).GetAwaiter().GetResult();
            if (vehicle == null) throw new SerializationException("Bad vehicle serialization");

            Console.WriteLine($"\nId: {vehicle.Id}, Type: {vehicle.VehicleType}, Balance:{vehicle.Balance}");
        }

        // 10
        public void TopUpVehicle()
        {
            var vehicles = _vehiclesProvider.GetVehicles().GetAwaiter().GetResult();
            if (vehicles == null)
                throw new SerializationException("Bad vehicles serialization");
            DisplayVehicleTable(vehicles.ToList());

            Console.Write("\nSelect vehicle for top up by ID: ");
            var id = InputValidators.StringInput();
            if (!id.IsMatchVehicleIdStringFormat()) throw new FormatException("Bad id format");

            Console.Write("Enter top up amount: ");
            var sum = InputValidators.DecimalInput(0m);
            var a = _transactionsProvider.TopUpVehicleById(id, sum).GetAwaiter().GetResult();
            if (a == null) throw new SerializationException("Bad vehicle serialization");

            Console.WriteLine("Vehicle topped up.");
        }

        //addictional methods
        private static void ShowTransactionsWithPagination(IReadOnlyCollection<string> transactions)
        {
            var pageNumber = 1;
            const int pageSize = 50;
            while (true)
            {
                Console.Clear();
                transactions.Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize).ForEach(Console.WriteLine);
                Console.WriteLine($"\nCurrent page is {pageNumber}. Select an option:\n" +
                                  "1. Previous\n" +
                                  "2. Next\n" +
                                  "3. Back to menu\n");
                var option = InputValidators.IntegerInput(1, 3);
                switch (option)
                {
                    case 3:
                        return;
                    case 2 when (pageNumber - 1) * pageSize > transactions.Count - pageSize:
                        break;
                    case 2:
                        pageNumber++;
                        break;
                    case 1 when pageNumber - 1 < 1:
                        break;
                    case 1:
                        pageNumber--;
                        break;
                }
            }
        }


        private static void DisplayVehicleTable(List<Vehicle> vehicles)
        {
            var counter = 1;
            Console.WriteLine($"|{"#",-3}|{"ID",-10}|{"Balance",-10}|{"Vehicle Type",-12}|");
            Console.WriteLine("________________________________________");
            foreach (var vehicle in vehicles)
                Console.WriteLine($"|{counter++,-3}|{vehicle.Id,-10}|{vehicle.Balance,-10}|{vehicle.VehicleType,-12}|");
        }
    }
}