﻿using System.Text.Json.Serialization;

namespace CoolParking.Client.Models
{
    public class Vehicle
    {
        [JsonPropertyName("id")] public string Id { get; set; }

        [JsonPropertyName("vehicleType")] public VehicleType VehicleType { get; set; }

        [JsonPropertyName("balance")] public decimal Balance { get; set; }
    }
}