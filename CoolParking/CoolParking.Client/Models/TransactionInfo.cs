﻿using System;
using System.Text.Json.Serialization;

namespace CoolParking.Client.Models
{
    public class TransactionInfo
    {
        [JsonPropertyName("vehicleId")] public string VehicleId { get; set; }
        [JsonPropertyName("sum")] public decimal Sum { get; set; }
        [JsonPropertyName("transactionDate")] public DateTime TransactionDate { get; set; }

        public override string ToString()
        {
            return
                $"{TransactionDate:MM/dd/yyyy hh:mm:ss tt}: {Sum} money withdrawn from vehicle with Id='{VehicleId}'.";
        }
    }
}