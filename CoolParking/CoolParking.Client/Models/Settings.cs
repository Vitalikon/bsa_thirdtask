﻿namespace CoolParking.Client.Models
{
    public static class Settings
    {
        public const string IdFormatRegEx = @"^([A-Z]{2})-([0-9]{4})-([A-Z]{2})$";
        public const string BaseDevelopmentUri = "https://localhost:5001/api/";
    }
}