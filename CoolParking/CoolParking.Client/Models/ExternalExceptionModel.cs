﻿using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace CoolParking.Client.Models
{
    public class ExternalExceptionModel
    {
        [JsonPropertyName("title")] public string Title { get; set; }

        [JsonPropertyName("status")] public int Status { get; set; }

        [JsonPropertyName("errors")] public Dictionary<string, List<string>> Errors { get; set; }

        public override string ToString()
        {
            var (key, value) = Errors.First();
            return $"\n{key}: {value.Aggregate((s1, s2) => s1 + "\n" + s2)}";
        }
    }
}