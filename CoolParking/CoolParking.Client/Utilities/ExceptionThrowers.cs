﻿using System;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using CoolParking.Client.Models;

namespace CoolParking.Client.Utilities
{
    public static class ExceptionThrowers
    {
        public static void ThrowExceptionFromResponseBody(string content)
        {
            var deserializedException = JsonSerializer.Deserialize<ExternalExceptionModel>(content);
            if (deserializedException == null)
                throw new ArgumentNullException("deserializedException", "Exception deserialization failed!");

            throw new HttpRequestException(deserializedException.ToString(), null,
                (HttpStatusCode) deserializedException.Status);
        }
    }
}