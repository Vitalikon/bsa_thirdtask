﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using CoolParking.Client.Models;
using CoolParking.Client.Utilities;

namespace CoolParking.Client.Providers
{
    public class TransactionsProvider
    {
        private readonly HttpClient _httpClient;


        public TransactionsProvider(HttpClient httpClient, string baseUri)
        {
            _httpClient = httpClient;
            _httpClient.BaseAddress = new Uri(baseUri + "transactions/");
        }


        public async Task<List<string>> GetAllTransactionsAsync()
        {
            var response = await _httpClient.GetAsync("all");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode) ExceptionThrowers.ThrowExceptionFromResponseBody(content);

            return content.Trim('"').Split("\\n", StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public async Task<IEnumerable<TransactionInfo>> GetLastTransactionsAsync()
        {
            var response = await _httpClient.GetAsync("last");
            var content = await response.Content.ReadAsStringAsync();
            if (content == "[]" || string.IsNullOrWhiteSpace(content))
                throw new KeyNotFoundException("No transactions data!");

            if (!response.IsSuccessStatusCode) ExceptionThrowers.ThrowExceptionFromResponseBody(content);

            return JsonSerializer.Deserialize<IEnumerable<TransactionInfo>>(content);
        }

        public async Task<Vehicle> TopUpVehicleById(string id, decimal sum)
        {
            var jsonTopUp = JsonSerializer.Serialize(new {Id = id, Sum = sum});
            var requestResponse = await _httpClient.PutAsync("topUpVehicle",
                new StringContent(jsonTopUp, Encoding.UTF8, "application/json"));
            var content = await requestResponse.Content.ReadAsStringAsync();

            if (!requestResponse.IsSuccessStatusCode) ExceptionThrowers.ThrowExceptionFromResponseBody(content);

            return JsonSerializer.Deserialize<Vehicle>(content);
        }
    }
}