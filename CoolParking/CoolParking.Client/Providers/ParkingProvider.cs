﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.Client.Providers
{
    public class ParkingProvider
    {
        private readonly HttpClient _httpClient;


        public ParkingProvider(HttpClient httpClient, string baseUri)
        {
            _httpClient = httpClient;
            _httpClient.BaseAddress = new Uri(baseUri + "parking/");
        }

        public async Task<decimal> GetBalance()
        {
            var response = await _httpClient.GetAsync("balance/");
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            var tryParse = decimal.TryParse(content, out var result);
            if (tryParse == false) throw new HttpRequestException("Failed to get a balance");

            return result;
        }

        public async Task<int> GetCapacity()
        {
            var response = await _httpClient.GetAsync("capacity");
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            var tryParse = int.TryParse(content, out var result);
            if (tryParse == false) throw new HttpRequestException("Failed to get a capacity");

            return result;
        }


        public async Task<int> GetFreePlaces()
        {
            var response = await _httpClient.GetAsync("freePlaces");
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            var tryParse = int.TryParse(content, out var result);
            if (tryParse == false) throw new HttpRequestException("Failed to get a free places");

            return result;
        }
    }
}