﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using CoolParking.Client.Models;
using CoolParking.Client.Utilities;

namespace CoolParking.Client.Providers
{
    public class VehiclesProvider
    {
        private readonly HttpClient _httpClient;


        public VehiclesProvider(HttpClient httpClient, string baseUri)
        {
            _httpClient = httpClient;
            _httpClient.BaseAddress = new Uri(baseUri + "vehicles/");
        }

        public async Task<IEnumerable<Vehicle>> GetVehicles()
        {
            var response = await _httpClient.GetAsync("");
            var content = await response.Content.ReadAsStringAsync();
            if (content == "[]" || string.IsNullOrWhiteSpace(content))
                throw new KeyNotFoundException("No vehicles data!");

            if (!response.IsSuccessStatusCode) ExceptionThrowers.ThrowExceptionFromResponseBody(content);

            return JsonSerializer.Deserialize<IEnumerable<Vehicle>>(content);
        }

        public async Task<Vehicle> GetVehicleById(string id)
        {
            var response = await _httpClient.GetAsync($"{id}");
            var content = await response.Content.ReadAsStringAsync();
            if (content == "[]" || string.IsNullOrWhiteSpace(content))
                throw new KeyNotFoundException("No vehicle data!");

            if (!response.IsSuccessStatusCode) ExceptionThrowers.ThrowExceptionFromResponseBody(content);

            return JsonSerializer.Deserialize<Vehicle>(content);
        }

        public async Task<Vehicle> CreateVehicle(Vehicle vehicle)
        {
            var jsonVehicle = JsonSerializer.Serialize(vehicle);
            var requestResponse =
                await _httpClient.PostAsync("", new StringContent(jsonVehicle, Encoding.UTF8, "application/json"));
            var content = await requestResponse.Content.ReadAsStringAsync();

            if (!requestResponse.IsSuccessStatusCode) ExceptionThrowers.ThrowExceptionFromResponseBody(content);

            return JsonSerializer.Deserialize<Vehicle>(content);
        }

        public async Task<bool> DeleteVehicleById(string id)
        {
            var requestResponse = await _httpClient.DeleteAsync($"{id}");
            var content = await requestResponse.Content.ReadAsStringAsync();
            if (!requestResponse.IsSuccessStatusCode) ExceptionThrowers.ThrowExceptionFromResponseBody(content);

            return requestResponse.StatusCode == HttpStatusCode.NoContent;
        }
    }
}