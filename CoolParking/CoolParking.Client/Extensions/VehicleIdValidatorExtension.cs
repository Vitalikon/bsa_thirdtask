﻿using System.Text.RegularExpressions;
using CoolParking.Client.Models;

namespace CoolParking.Client.Extensions
{
    public static class VehicleIdValidatorExtension
    {
        public static bool IsMatchVehicleIdStringFormat(this string str)
        {
            if (str.Length != 10 || string.IsNullOrWhiteSpace(str)) return false;

            return Regex.IsMatch(str, Settings.IdFormatRegEx);
        }
    }
}