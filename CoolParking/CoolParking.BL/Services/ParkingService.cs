﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Timers;
using CoolParking.BL.Extensions;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly Parking _parking;
        private readonly List<TransactionInfo> _transactionInfos;
        private readonly ITimerService _withdrawTimerService;
        private readonly ITimerService _loggerTimerService;
        private readonly ILogService _logger;

        public ParkingService(ITimerService withdrawTimer, ITimerService loggerTimer, ILogService logger)
        {
            _parking = Parking.GetInstance();
            _parking.Balance = Settings.InitialParkingBalance;
            _logger = logger;
            _loggerTimerService = loggerTimer;
            _loggerTimerService.Interval = Settings.LogInterval;
            _loggerTimerService.Elapsed += LoggerTimerElapsed;
            _loggerTimerService.Start();
            _withdrawTimerService = withdrawTimer;
            _withdrawTimerService.Interval = Settings.PaymentInterval;
            _withdrawTimerService.Elapsed += WithdrawTimerElapsed;
            _withdrawTimerService.Start();
            _transactionInfos = new List<TransactionInfo>();
        }

        public ParkingService()
        {
            _parking = Parking.GetInstance();
            _parking.Balance = Settings.InitialParkingBalance;
            _logger = new LogService(Settings.LoggerFilePath);
            _loggerTimerService = new TimerService {Interval = Settings.LogInterval};
            _loggerTimerService.Elapsed += LoggerTimerElapsed;
            _loggerTimerService.Start();
            _withdrawTimerService = new TimerService {Interval = Settings.PaymentInterval};
            _withdrawTimerService.Elapsed += WithdrawTimerElapsed;
            _withdrawTimerService.Start();
            _transactionInfos = new List<TransactionInfo>();
        }

        private void LoggerTimerElapsed(object sender, ElapsedEventArgs e)
        {
            if (_transactionInfos.Count > 0)
            {
                var builder = new StringBuilder();
                _transactionInfos.ForEach(info => builder.Append(info + "\n"));
                _logger.Write(builder.ToString());
            }
            else
                _logger.Write(string.Empty);

            _transactionInfos.Clear();
        }

        private void WithdrawTimerElapsed(object sender, ElapsedEventArgs e)
        {
            foreach (var vehicle in _parking.Vehicles)
            {
                var profit = CalculateProfit(vehicle);
                vehicle.Balance -= profit;
                _parking.Balance += profit;
                _transactionInfos.Add(new TransactionInfo(profit, vehicle.Id));
            }
        }

        private static decimal CalculateProfit(Vehicle vehicle)
        {
            var tariff = Settings.VehicleTariffs[vehicle.VehicleType];
            if (vehicle.Balance <= 0m)
            {
                return tariff * Settings.PenaltyMultiplier;
            }

            if (vehicle.Balance >= tariff)
            {
                return tariff;
            }

            if (vehicle.Balance - tariff < 0m)
            {
                var negativeBalance = (vehicle.Balance - tariff) * Settings.PenaltyMultiplier - vehicle.Balance;
                return negativeBalance * (-1m);
            }

            return tariff - vehicle.Balance + (tariff - vehicle.Balance) * Settings.PenaltyMultiplier;
        }

        public void Dispose()
        {
            _parking.Clear();
            _loggerTimerService.Dispose();
            _withdrawTimerService.Dispose();
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Vehicles.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.Vehicles.Capacity - _parking.Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _parking.Vehicles.AsReadOnly();
        }


        public Vehicle GetVehicle(string id)
        {
            var vehicle = GetVehicles().FirstOrDefault(v => v.Id == id);
            if (vehicle == null)
                throw new KeyNotFoundException("Vehicle not found!");
            return vehicle;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.Vehicles.Count == Settings.ParkingCapacity)
            {
                throw new InvalidOperationException("Trying to add a vehicle on full parking!",
                    new OverflowException());
            }

            if (_parking.Vehicles.Exists(v => v.Id == vehicle.Id))
            {
                throw new ArgumentException($"Vehicle with Id {vehicle.Id} is already in the parking!");
            }

            _parking.Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);
            if (vehicle == null)
                throw new ArgumentException("Vehicle not found!", new KeyNotFoundException());
            if (vehicle.Balance < 0m)
            {
                throw new InvalidOperationException(
                    $"Trying to remove vehicle with negative balance: {vehicle.Balance}!");
            }

            _parking.Remove(vehicleId);
        }


        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0m)
            {
                throw new ArgumentException("Sum cannot be less than 0!");
            }

            if (!vehicleId.IsMatchVehicleIdStringFormat())
            {
                throw new ArgumentException("Bad Vehicle Id format!", new FormatException());
            }

            var vehicle = _parking.Vehicles.FirstOrDefault(x => x.Id == vehicleId);
            if (vehicle == null)
            {
                throw new ArgumentException("Vehicle not found!", new KeyNotFoundException());
            }

            vehicle.Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactionInfos.ToArray();
        }

        public string ReadFromLog()
        {
            return _logger.Read();
        }
    }
}