﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public sealed class Parking
    {
        private static readonly Lazy<Parking> Lazy = new(() => new Parking());
        public decimal Balance { get; set; }
        public List<Vehicle> Vehicles { get; set; }

        private Parking()
        {
            Balance = Settings.InitialParkingBalance;
            Vehicles = new List<Vehicle>(Settings.ParkingCapacity);
        }

        public void Clear()
        {
            Vehicles.Clear();
            Vehicles = new List<Vehicle>(Settings.ParkingCapacity);
            Balance = Settings.InitialParkingBalance;
        }

        public void Remove(string vehicleId)
        {
            Vehicles.RemoveAll(vehicle => vehicle.Id == vehicleId);
        }
        

        public static Parking GetInstance() => Lazy.Value;
    }
}