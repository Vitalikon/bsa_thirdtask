﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal InitialParkingBalance = 0M;
        public const int ParkingCapacity = 10;
        public const int PaymentInterval = 5000;
        public const int LogInterval = 60000;
        public const decimal PenaltyMultiplier = 2.5M;
        public static readonly string LoggerFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
        public const string IdFormatRegEx = @"^([A-Z]{2})-([0-9]{4})-([A-Z]{2})$";
        public static readonly Dictionary<VehicleType, decimal> VehicleTariffs = new()
        {
            {VehicleType.PassengerCar, 2M},
            {VehicleType.Truck, 5M},
            {VehicleType.Bus, 3.5M},
            {VehicleType.Motorcycle, 1M}
        };
    }
}