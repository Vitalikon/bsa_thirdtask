## Welcome!

В существующем солюшене (можно в отдельном репозитории) создать WebAPI проект на основе .NET 5, с именем "CoolParking.WebAPI" (путь должен получиться /CoolParking/CoolParking.WebAPI, аналогично /CoolParking/CoolParking.BL - важно для автопроверки).  

**В этом проекте необходимо реализовать следующие эндпоинты:**  
  
**GET api/parking/balance**  
Response:   
If request is handled successfully - Status Code: 200 OK   
Body schema: decimal   
Body example: 10.5   
  
**GET api/parking/capacity**   
Response:   
If request is handled successfully - Status Code: 200 OK   
Body schema: int   
Body example: 10   
  
**GET api/parking/freePlaces**   
Response:   
If request is handled successfully - Status Code: 200 OK   
Body schema: int   
Body example: 9   
  
**GET api/vehicles**   
Response:   
If request is handled successfully - Status Code: 200 OK   
Body schema: [{ “id”: string, “vehicleType”: int, "balance": decimal }]   
Body example: [{ “id”: “GP-5263-GC”, “vehicleType”: 2, "balance": 196.5 }]   
  
**GET api/vehicles/id (id - vehicle id of format “AA-0001-AA”)**   
Response:   
If id is invalid - Status Code: 400 Bad Request   
If vehicle not found - Status Code: 404 Not Found   
If request is handled successfully - Status Code: 200 OK   
Body schema: { “id”: string, “vehicleType”: int, "balance": decimal }   
Body example: { “id”: “GP-5263-GC”, “vehicleType”: 2, "balance": 196.5 }   
  
**POST api/vehicles**   
Request:   
Body schema: { “id”: string, “vehicleType”: int, “balance”: decimal }   
Body example: { “id”: “LJ-4812-GL”, “vehicleType”: 2, “balance”: 100 }   
Response:   
If body is invalid - Status Code: 400 Bad Request   
If request is handled successfully - Status Code: 201 Created   
Body schema: { “id”: string, “vehicleType”: int, "balance": decimal }   
Body example: { “id”: “LJ-4812-GL”, “vehicleType”: 2, "balance": 100 }   
  
**DELETE api/vehicles/id (id - vehicle id of format “AA-0001-AA”)**   
Response:   
If id is invalid - Status Code: 400 Bad Request   
If vehicle not found - Status Code: 404 Not Found   
If request is handled successfully - Status Code: 204 No Content   
  
**GET api/transactions/last**   
Response:   
If request is handled successfully - Status Code: 200 OK   
Body schema: [{ “vehicleId”: string, “sum”: decimal, "transactionDate": DateTime }]   
Body example: [{ “vehicleId”: "DG-3024-UB", “sum”: 3.5, "transactionDate": "2020-05-10T11:36:20.6395402+03:00"}]   
  
**GET api/transactions/all (только транзакции с лог файла)**   
Response:   
If log file not found - Status Code: 404 Not Found   
If request is handled successfully - Status Code: 200 OK   
Body schema: string   
Body example: “5/10/2020 11:21:20 AM: 3.50 money withdrawn from vehicle with Id='GP-5263-GC'.\n5/10/2020 11:21:25 AM: 3.50 money withdrawn from vehicle with Id='GP-5263-GC'.”   
  
**PUT api/transactions/topUpVehicle**   
Body schema: { “id”: string, “Sum”: decimal }   
Body example: { “id”: “GP-5263-GC”, “Sum”: 100 }    
Response:   
If body is invalid - Status Code: 400 Bad Request   
If vehicle not found - Status Code: 404 Not Found   
If request is handled successfully - Status Code: 200 OK   
Body schema: { “id”: string, “vehicleType”: int, "balance": decimal }   
Body example: { “id”: “GP-5263-GC”, “vehicleType”: 2, "balance": 245.5 }   
  
**Требования по реализации**   
  
Старт Паркинга необходимо выполнить в классе Startup.   
На клиенте использовать System.HttpClient для работы с запросами.   
Использовать JSON как формат данных для общения между клиентом и сервером.   
  
**Рекомендационное**   
  
Проект клиента может быть в одном солюшене с “Бэком” - VS Studio позволяет запускать одновременно несколько проектов из солюшена.   
Не усложнять - не нужно присоединять БД, разрабатывать Web клиентское приложение (Angular, React) и .т.п.   
К респонсам 4** добавляй message.   